package main;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import ejercicio_one_to_many.Estudiante;
import ejercicio_one_to_many.Libro;

public class Estudiante_Libro {

	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("persistence");
		EntityManager em = emf.createEntityManager();
		
		List<Libro> lista_libros = new ArrayList();
		Estudiante e1 = new Estudiante();
		e1.setNombre("Pedro");
		
		Libro l1 = new Libro();
		l1.setNombre("ingles");
		l1.setEstudiante(e1);;
		e1.setLibro(lista_libros);
		
		Libro l2 = new Libro();
		l2.setNombre("matematica");
		l2.setEstudiante(e1);
		e1.setLibro(lista_libros);
		
		Libro l3 = new Libro();
		l3.setNombre("analisis");
		l3.setEstudiante(e1);;
		e1.setLibro(lista_libros);
		
		Libro l4 = new Libro();
		l4.setNombre("base");
		l4.setEstudiante(e1);
		e1.setLibro(lista_libros);		
		
		lista_libros.add(l1);
		lista_libros.add(l2);
		lista_libros.add(l3);
		lista_libros.add(l4);
		
		
		Metodo_E_L mel= new Metodo_E_L();
		mel.miMetodo(e1, lista_libros);
		
		
		}
}
