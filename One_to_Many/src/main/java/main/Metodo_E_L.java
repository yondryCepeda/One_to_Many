package main;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import ejercicio_one_to_many.*;

public class Metodo_E_L {

	public void miMetodo(Estudiante es, List<Libro> li){
		EntityManagerFactory emf =  Persistence.createEntityManagerFactory("persistence");
		EntityManager em = emf.createEntityManager();
		
		//lista para obetener los libros y asi compararla con la lista nueva y ver que libros existen en la base de datos
		List<Libro> libros_lista = em.createNamedQuery("Libro.obtenerLibros", Libro.class).getResultList();
		
		//lista para obtener todos los estudiantes, usada para verificar si el estudiante existe
		List<Estudiante> estudiante_lista = em.createNamedQuery("Estudiante.obtenerEstudiante", Estudiante.class).getResultList();
		
		//lista para asignar los libros nuevos
		List <Libro> libros_nuevos = new ArrayList();
		
		try{
		em.getTransaction().begin();
			//verificar si el estudiante existe
				if(estudiante_lista.contains(es)){
					System.out.println("el estudiante "+es.getNombre()+" ya existe!");		
				}else if(estudiante_lista.contains(es)==false){
					System.out.println("se agrego el nuevo estudiante "+es);
					em.persist(es);
				}
				//bucle hecho para saber los libros existentes
			for(int i=0; i<estudiante_lista.size(); i++){
				List libros_estudiante = em.createNamedQuery("Libro.obtenerLibros2", Libro.class)
						.setParameter("estudiante", estudiante_lista.get(i))
						.getResultList();
				if(estudiante_lista.get(i).equals(es)){
				System.out.println("libros existentes: "+libros_estudiante);
					}
				//sub-bucle hecho para hacer un update y asignar los libros nuevos
			}
			//buscar estudiante por nombre
			Estudiante libro_estudiante = 
					em.createNamedQuery("Estudiante.obtenerEstudiante2", Estudiante.class)
					.setParameter("nombre", es.getNombre())
					.getSingleResult();
			//obtener libros de dicho estudiante
			List libros_estudiante2 = em.createNamedQuery("Libro.obtenerLibros2", Libro.class)
					.setParameter("estudiante", libro_estudiante)
					.getResultList();
			//condicion para que no inserte datos duplicados
			if(estudiante_lista.contains(es)){
				
				for(int a=0; a<li.size(); a++){
					if(libros_estudiante2.contains(li.get(a)) != true){
					Libro libros = new Libro();
					libros.setNombre((li.get(a).getNombre()));
					libros.setEstudiante(libro_estudiante);

					libros_nuevos.add(libros);
					
					int id = libro_estudiante.getId();
					//update para agregar libros nuevos
					Estudiante estudiante = em.find(Estudiante.class, id);
					estudiante.setLibro(libros_nuevos);
					System.out.println("libros nuevos: "+libros);
					
						}
					}
				}
			
				
		em.getTransaction().commit();	
			
		}catch (Exception e) {
		e.printStackTrace();
		em.getTransaction().rollback();
		}
		
		
		
	}
}
